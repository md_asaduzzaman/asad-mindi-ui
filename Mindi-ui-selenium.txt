package HappyPath;

import org.testng.annotations.Test;

import junit.framework.Assert;

import org.testng.annotations.BeforeClass;
 
import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
 
 
 

public class RunTestHappyPath{

	WebDriver driver;
	//AppLibrary applib;
	AboutYou cAboutYou;
	Exterior cExterior;
	Interior cInterior;
	AdditionalInformation cAdditionalInformation;
	SSNinformation cSSNinformation;
	YourQuote cYourQuote;
	Purchase cPurchase;
	Welcome cWelcome;
	@BeforeClass
	public void RunTest() throws InterruptedException{
		
		System.setProperty("webdriver.chrome.driver", new File("chromedriver.exe").getAbsolutePath());
		driver = new ChromeDriver();
		driver.get("https://uat-patmindi-awsapi.homesite.com/sales/web/?r=homesalesqa&zip=80002&expid=1061");
		Thread.sleep(5000);
		driver.manage().window().maximize();
		 
		 
		Thread.sleep(5000);
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//System.out.println(driver.getTitle());
		
		//applib.getDriverInstance(browsertype);
		cAboutYou = new AboutYou(driver);
		 
		
		cExterior = new Exterior(driver);
		cInterior = new Interior(driver);
		cAdditionalInformation = new AdditionalInformation(driver);
		cSSNinformation = new SSNinformation(driver);
		cYourQuote	= new YourQuote(driver);
		cPurchase = new Purchase(driver);
		cWelcome = new Welcome(driver);
		 
	}
		 
//@Parameters({"FirstName","LastName","DateOfBirth","EmailAddress","PhoneNumber","Youraddress","city"})
@Test
public void verifyaboutyou() throws Exception
//String FirstName,String LastName,String DateOfBirth,String EmailAddress,String PhoneNumber,String Youraddress, String city
	{
	 
			//AboutYou Class
	  cAboutYou.TellUsAboutYou();//FirstName,LastName,DateOfBirth,EmailAddress,PhoneNumber
		  cAboutYou.WellsFargoAccount();
		  cAboutYou.YourAddress();  //Youraddress, city
		  cAboutYou.NewHomePurchase();
		  cAboutYou.StartQuote();
		  cAboutYou.waitforsometime();
		  
		  
		  
		 
		 //Exterior Class
		 
		cExterior.SfamilyOrMfamily();
		//=========================================================================================================================================
		String propertyRecord =driver.findElement(By.cssSelector("span[class*='label__text__left label__text__left--prefill']")).getText();
		if(!propertyRecord.equalsIgnoreCase("Property Records found")){
			Assert.fail();
			System.out.println("Property Records found text not found but found ="+ propertyRecord);
		}else{
			System.out.println("Property Records found text");
		}
		//========================================================================================================================================
		
		
		cExterior.StyleOfYourHome();
		cExterior.HomeInfo();
		cExterior.Slope();
		cExterior.ExteriorSiding();
		cExterior.Foundation();
		cExterior.Garage();
		cExterior.Roof();
		cExterior.Rmeterial();
		cExterior.RoofInstall();
		cExterior.Continue();
		
		  
		  
		//Interior Class
		
		cInterior.InsideWallMaterial();
		cInterior.Ftype();
		cInterior.Ceiling();
		cInterior.Heating();
		cInterior.KitchenMaterial();
		cInterior.next();
		cInterior.waitforsometime();
	 
		
		
		 //Additional Information Class
		 
		cAdditionalInformation.PoliceStartDate();
		cAdditionalInformation.Primary();
		cAdditionalInformation.AddPerson();
		cAdditionalInformation.conti();
		cAdditionalInformation.waitforsometime();
		
		 
		
		
		//SSNinformation (Addition Information Page)
		
		cSSNinformation.SSnumber();
		cSSNinformation.PAddress();
		cSSNinformation.Conti();
		
		
		//Your Quote
		
		cYourQuote.ChooseYourCoverage();
				
				
		 
		//Purchase
		
		cPurchase.MailingAddress();
		cPurchase.Morgage();
		//cPurchase.amount();
		cPurchase.Payment();
		cPurchase.creditInfo();
		cPurchase.completeP();
		
		
		//Welcome
		 cWelcome.SendVcode();
		 
		}
	}
