package HappyPath;

/*
Fill out  the below fields Additional Information page and continue

* What is your desired new policy start date?
  Enter date as tomorrow's date MM/DD/YYYY
 
* How will you be using your home?
  Primary Home (Owner Occupied)
* Do you currrently have insurance on this propery as ? YES
* Do you want to add a person as No
 
* Continue

*/
 

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AdditionalInformation {
	WebDriver driver;
	
	public AdditionalInformation(WebDriver driver){
		
		this.driver = driver;
	}
	public  void Information(String NewPolicyStartdate){
		//String NewPolicyStartdate 
		waitforsometime();
		// What is your desired new policy start date?
		driver.findElement(By.className("textbox__input--EffectiveDateOfPolicy textbox__input__error")).sendKeys(NewPolicyStartdate);
		waitforsometime();
		// how will you be using your home
		driver.findElement(By.xpath("//*[@id='mindi']/div[1]/div[3]/div[5]/div[2]/div/div[1]/button/div[1]/div"));
		
		//Do you want to add a person to your policy as a secondary insured
		driver.findElement(By.xpath("//*[@id='mindi']/div[1]/div[3]/div[29]/div[2]/div/div[2]/button/div[1]/div"));
		waitforsometime();
		
		 
		
		//Continue
		driver.findElement(By.xpath("//*[@id='mindi']/div[1]/div[3]/div[31]/div[2]/button")).click();
		
		 
		
	}

	public void waitforsometime(){
		
		try{
		Thread.sleep(500);	
			
		}
		catch (InterruptedException e){
			e.printStackTrace();
		}
		
		
	}



		  }

