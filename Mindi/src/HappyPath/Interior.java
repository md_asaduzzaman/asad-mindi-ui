package HappyPath;

/*
/* Fill out all the fields in Home Interior page and continue 
* TestData
*  Inside wall material(s)
 Drywall / Veneer Plaster 
* Floor type(s)
 Carpet 
* Ceiling height
 8 feet or less (most common)
* Primary heating type
 Electric
* Kitchen countertop material
 Plastic Laminate (Formica�) 
* Electrical wiring type
 Circuit Breakers 

*/

 

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Interior {
	WebDriver driver;
	
	public Interior(WebDriver driver){
		
		this.driver = driver;
	}
	public void InsideWallMaterial(){
		// Inside wall material(s)
		driver.findElement(By.xpath("//*[@id='InsideWallMaterial']/div[4]/div[1]/div/a[1]/div/div[2]/div/div"));
		//Floor type(s)
		waitforsometime();
		driver.findElement(By.xpath("//*[@id='FloorType']/div[3]/div[1]/div/a[1]/div/div[2]/div/div"));
		//Ceiling height
		
		driver.findElement(By.xpath("//*[@id='CeilingHeight']/div[4]/div[1]/div/a[1]/div/div/div/div"));
		/*//for the majority of the home
		driver.findElement(By.className("counter__input")).sendKeys("1");
		//How many rooms have cathedral or vaulted ceilings? 
		driver.findElement(By.className("counter__input")).sendKeys("2");
	 //How many rooms have crown molding ? 
		 driver.findElement(By.className("itemList__radio__inner"));
		 */
		waitforsometime();
		 // Primary heating type
	 driver.findElement(By.xpath("//*[@id='HeatingType']/div[3]/div[1]/div/a[3]/div/div/div/div"));
	 //Number of fireplaces
	// driver.findElement(By.className("itemList__radio__inner"));
	 waitforsometime();
   //Kitchen countertop material
	driver.findElement(By.xpath("//*[@id='KitchenCounterTopMaterial']/div[2]/div[1]/div/a[1]/div/div[2]/div/div"));
   //Number of full baths
	//driver.findElement(By.className("counter__input")).sendKeys("1");
 //Number of half baths
	//driver.findElement(By.className("button__button")).click();
	
	//Electrical wiring type
   driver.findElement(By.xpath("//*[@id='ElectricalWiringType']/div[3]/div[1]/div/a[1]/div/div[2]/div/div"));
   waitforsometime();
  //Next
  driver.findElement(By.xpath("//*[@id='mindi']/div[1]/div[2]/div/div[2]/button")).click();
  
 
	}
	
	
public void waitforsometime(){
	
	try{
	Thread.sleep(1000);	
		
	}
	catch (InterruptedException e){
		e.printStackTrace();
	}
	
	
}



	  }
