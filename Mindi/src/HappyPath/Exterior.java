package HappyPath;


/*
 *Fill out all the  Exterior page field and continue
 
 *TestData

 * Single-Family
* Select Home style as Ranch/Colonial
* leave pre fill section as it is.
* Select slope? as No
* Type of exterior siding:
   Vinyl siding 
* Type of foundation
   Closed Basement 0%
* Type of Garage No
* Roof shape as Gable
* Roof material as Asphalt shingles
* Enter What year was the Roof installed as 2012
 */





 

 

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
 

public class Exterior {
	 
	WebDriver driver;
	
	public Exterior(WebDriver driver){
		this.driver = driver;
	}
	
	 
	
	public  void HomeExterior( ){
		
		 
	 
		 
	//Is your home single-family or multi-family? Single-Family
		
		driver.findElement(By.className("itemList__radio__outer--single"));
		waitforsometime();
		//What is the styleof your home? Ranch
		
		driver.findElement(By.xpath("//*[@id='HomeArchitecturalStyle']/div[3]/div[1]/div/a[1]/div/div[2]/div/div"));
		//Original year of construction:
		
		 driver.findElement(By.xpath("//*[@id='mindi']/div[1]/div[2]/div[5]/input")).sendKeys("");
		 //Approximate living area square footage:
		 
		 driver.findElement(By.xpath("//*[@id='mindi']/div[1]/div[2]/div[6]/input")).sendKeys("");
		 waitforsometime();
		 //Approximate market value: 
		 driver.findElement(By.xpath("//*[@id='mindi']/div[1]/div[2]/div[7]/input")).sendKeys("");
       //Is the home on a slope
		 
		 driver.findElement(By.xpath("//*[@id='mindi']/div[1]/div[2]/div[8]/div[2]/div/div[2]/button/div[1]/div"));
		 //Type of exterior siding
		 driver.findElement(By.xpath("//*[@id='ExteriorSiding']/div[4]/div[1]/div/a[1]/div/div[2]/div/div"));
		 //Type of foundation
		 driver.findElement(By.xpath("//*[@id='FoundationType']/div[3]/div[1]/div/a[1]/div/div/div/div"));
		 waitforsometime();
		// Approximately how much of your basement is finished?
		 driver.findElement(By.xpath("//*[@id='FinishedBasement']/div[2]/div[1]/div/a[1]/div/div/div/div"));
		 //Type of Garage
		 driver.findElement(By.xpath("//*[@id='GarageType']/div[2]/div[1]/div/a[1]/div/div/div/div"));
		// Roof shape
		 driver.findElement(By.xpath("//*[@id='RoofShape']/div[3]/div[1]/div/a[1]/div/div[2]/div/div"));
		//Roof material
		 driver.findElement(By.xpath("//*[@id='RoofMaterial']/div[3]/div[1]/div/a[1]/div/div[2]/div/div"));
		// What year was the roof installed? 
		 driver.findElement(By.className("//*[@id='mindi']/div[1]/div[2]/div[15]/input")).sendKeys("");
		 
		 driver.findElement(By.xpath("//*[@id='mindi']/div[1]/div[2]/div[16]/div[2]/button")).click();
		
 
	}

public void waitforsometime(){
		
		try{
		Thread.sleep(1000);	
			
		}
		catch (InterruptedException e){
			e.printStackTrace();
		}
		
		
}
	
	

		  }
