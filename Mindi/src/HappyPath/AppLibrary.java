package HappyPath;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.Parameters;
 
//@Parameters({"browsertype"})
public class AppLibrary {
	String browsertype ="FF";
    WebDriver driver;
    
    public void getDriverInstance(String browsertype)
    {
    
    if(browsertype.equals("FF"))
    {
                    System.out.println("in FF");
                    driver = new FirefoxDriver();
                    
    }
      else if (browsertype.equals("IE"))
    {
                    System.out.println("in IE");
                    System.setProperty("webdriver.ie.driver", new File("IEDriverServer.exe").getAbsolutePath());

driver = new InternetExplorerDriver();
}
    else 
    {
                    
                    System.setProperty("webdriver.chrome.driver", new File("chromedriver.exe").getAbsolutePath());
driver = new ChromeDriver();
System.out.println("in CH");

}
     
}
}
